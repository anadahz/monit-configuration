# Monit configuration

This repository contains a configuration file for
[monit](https://mmonit.com/monit/).  We use monit to monitor the infrastructure
of Tor's anti-censorship team.

## Set up a monit instance

1. Install [monit](https://mmonit.com/monit/).

2. Adapt the monit configuration file from this repository to your needs.  You
   will have to create the file `/home/admin/.monit-email-config`, which
   contains monit's SMTP configuration to send outage alerts.  The comment in
   the configuration file goes into more detail.

   You may also have to adapt the path
   `/home/admin/rcs/monit-configuration/scripts/bridgedb-email-test`.
   Finally, add the Gmail password to the variable `PASSWORD` in
   `scripts/bridgedb-email-test`.

3. Start monit using this repository's configuration file:

         /path/to/monit -c /path/to/monitrc

4. Optionally: Make sure that your monit instance is started automatically after
   a reboot, e.g., by running `crontab -e` and adding the following line:

         @reboot /path/to/monit -c /path/to/monitrc
